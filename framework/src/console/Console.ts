import { EventEmitter } from 'events';
import has = require('lodash/fp/has');
import { createInterface as newReadLine, ReadLine } from 'readline';
import { Readable, Writable } from 'stream';
import * as tty from 'tty';
import { ConsoleContract, ConsoleState } from '../contract/ConsoleContract';
import { ConsoleOptions, DefaultConsoleOption } from './ConsoleOptions';

/**
 * @author Chaniwat Seangchai (chaniwat.meranote@gmail.com)
 * @since 0.1
 */
export class Console extends EventEmitter implements ConsoleContract {

    private _state: ConsoleState = ConsoleState.NOT_START;
    public get state(): ConsoleState {
        return this._state;
    }

    private _readline: ReadLine;
    private _inputStream: Readable;
    private _outputStream: Writable;

    constructor(option: ConsoleOptions = DefaultConsoleOption) {
        super();
        this._inputStream = option.input as Readable;
        this._outputStream = option.output as Writable;

        // If input is not exists, disable the ReadLine creation process.
        // Mean this Console can only produce output and will not read the user input.
        // (eg. for Logging and Monitoring)
        if (option.input === null) {
            this._state = ConsoleState.DISABLE;
        } else {
            this.prepareReadLine(option);
        }
    }

    /**
     * Prepare ReadLine interface.
     * @param {ConsoleOptions} option
     */
    protected prepareReadLine(option: ConsoleOptions) {
        // Create new ReadLine instance and pause for initialization
        this._readline = newReadLine(option);
        this._readline.pause();

        // If input is TTY (unix-like terminal)
        if (has(option.input as any, 'isTTY')) {
            // Cast to TTY and Intercept key event
            const input = option.input as tty.ReadStream;
            input.setRawMode(true);
            input.on('keypress', this.onKeyPressInput.bind(this));
        }

        // Register Event
        this._readline.on('line', this.onLineReadLine.bind(this));
        this._readline.on('SIGINT', this.onSIGINTReadLine.bind(this));
    }

    /**
     * Handler for 'keypress' Event on input TTY.
     */
    protected onKeyPressInput() {
        if (this._state !== ConsoleState.READ) {
            this.clearLine();
        }
    }

    /**
     * Handler for 'line' Event on ReadLine.
     * @param {string} data
     */
    protected onLineReadLine(data: string) {
        if (this._state !== ConsoleState.READ) {
            // If console is paused or stopped reading, stop emitting data,
            // And clear the user input (with the raw mode tty 'keypress')
            this._outputStream.write('\x1B[1A');
        } else {
            // console is reading, normally emitting data
            this.emit('line', data);
        }
    }

    /**
     * Handler for 'SIGINT' Event on ReadLine.
     */
    protected onSIGINTReadLine() {
        // Delegate event
        this.emit('SIGINT');

        switch (this._state) {
            // If console is reading, skip current line
            case ConsoleState.READ: this.clearReadLineBuffer(); break;
            // If console is pausing, explicitly resume reading
            case ConsoleState.PAUSE: this.resume(); break;
        }
    }

    public terminate(): boolean {
        if (this._state !== ConsoleState.DISABLE && this._readline) {
            this.stop();
            this._readline.close();
        }
        return true;
    }

    public start() {
        // If console is disabled, prevent the call
        if (this._state === ConsoleState.DISABLE) { return; }

        // If console is in the initial state, first start the readline
        if (this._state === ConsoleState.NOT_START) {
            this._readline.resume();
            this._state = ConsoleState.STOP;
        }

        // Delegate to resume
        this.resume();
    }

    public stop() {
        // If console is disabled, prevent the call
        if (this._state === ConsoleState.DISABLE) { return; } else { this._state = ConsoleState.STOP; }

    }

    public resume() {
        // If console is disabled, prevent the call
        if (this._state === ConsoleState.DISABLE) { return; }
        if ([ConsoleState.PAUSE, ConsoleState.STOP].indexOf(this._state) !== -1) {
            // Clear input buffer before resume
            this.clearReadLineBuffer();
            // Change State
            this._state = ConsoleState.READ;
            // Prompt the console
            this._readline.prompt();
        }
    }

    public pause() {
        // If console is disabled, prevent the call
        if (this._state === ConsoleState.DISABLE) { return; } else { this._state = ConsoleState.PAUSE; }
    }

    public println(data: string) {
        return this.print(data + '\r\n');
    }

    public print(data: string) {
        let lineBuffer;
        // Before print, if console is reading, save the current line buffer and clear it first
        if (this._state === ConsoleState.READ) {
            // If console not paused, keep the current line buffer first
            lineBuffer = (this._readline as any).line;
            // Then clear current line
            this.clearLine();
        }
        // Write data
        this._outputStream.write(data);
        // After print, if console is reading, Restore the current line buffer
        if (this._state === ConsoleState.READ) {
            // Prompt a console
            this._readline.prompt();
            // Then restore the current line buffer
            this._readline.write(lineBuffer);
        }
    }

    /**
     * Clear current ReadLine's line buffer.
     * (This doesn't clear the output text, only input buffer)
     */
    protected clearReadLineBuffer() {
        if (this._state !== ConsoleState.DISABLE) {
            (this._readline as any).line = '';
        }
    }

    /**
     * Clear the screen.
     */
    public clearScreen(): void {
        this.clearReadLineBuffer();
        this._outputStream.write('\x1Bc');
    }

    /**
     * Clear the current line.
     */
    public clearLine(): void {
        this.clearReadLineBuffer();
        this._outputStream.write('\x1B[2K\x1B[1000D');
    }

}

import { ReadLineOptions } from 'readline';

export interface ConsoleOptions extends ReadLineOptions {}

/**
 * Default option for Console
 */
export const DefaultConsoleOption: ConsoleOptions = {
    input: process.stdin,
    output: process.stdout,
};

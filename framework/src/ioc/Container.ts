import { Container as InversifyContainer } from 'inversify';
export { Types as IoC } from './Types';

export const Container = new InversifyContainer;

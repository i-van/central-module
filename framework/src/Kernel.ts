import merge = require('lodash/fp/merge');
import { Console } from './console/Console';
import { ConsoleOptions } from './console/ConsoleOptions';
import { ConsoleContract } from './contract/ConsoleContract';
import { Container, IoC } from './ioc/Container';
import { DefaultKernelOption, KernelOption } from './KernelOption';

/**
 * Application Kernel.
 *
 * @author Chaniwat Seangchai (chaniwat.meranote@gmail.com)
 * @since 0.1
 */
export class Kernel {

    /**
     * Application's Console
     * @type {ConsoleContract}
     * @private
     */
    private _console: ConsoleContract = null;

    constructor(option: KernelOption = {}) {
        option = merge(DefaultKernelOption, option);

        if (process.env.NO_CONSOLE !== 'true') {
            this.makeConsole({
                input: option.input,
                output: option.output,
            } as ConsoleOptions);
        }
    }

    protected makeConsole(option: ConsoleOptions) {
        let instance = this._console = new Console(option);
        Container.bind<ConsoleContract>(IoC.APPLICATION_CONSOLE).toConstantValue(instance);
        Container.bind<ConsoleContract>(IoC.CONSOLE).toConstantValue(instance);
    }
}

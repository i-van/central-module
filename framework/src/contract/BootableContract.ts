/**
 * Bootable Contract, Any class (or object) that implement this interface
 * will mark as Bootable instance and will be booted before application is started.
 *
 * @author Chaniwat Seangchai (chaniwat.meranote@gmail.com)
 * @since 0.1
 */
export interface BootableContract {
    boot(): boolean;
}

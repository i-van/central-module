/**
 * Terminatable Contract, Any class (or object) that implement this interface
 * will mark as terminatable instance and will be terminated before application is exited.
 *
 * @author Chaniwat Seangchai (chaniwat.meranote@gmail.com)
 * @since 0.1
 */
export interface TerminatableContract {
    terminate(): boolean;
}

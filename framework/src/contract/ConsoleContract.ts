import { TerminatableContract } from './TerminatableContract';

/**
 * Console Contract, Interface for Console.
 *
 * @author Chaniwat Seangchai (chaniwat.meranote@gmail.com)
 * @since 0.1
 */
export interface ConsoleContract extends TerminatableContract {
    state: ConsoleState;
    start();
    stop();
    resume();
    pause();
    print(data: string);
    println(data: string);
    clearLine();
    clearScreen();
    addListener(event: ConsoleEvent, listener: Function): this;
    on(event: ConsoleEvent, listener: Function): this;
    once(event: ConsoleEvent, listener: Function): this;
    removeListener(event: ConsoleEvent, listener: Function): this;
}

/**
 * Console Event, Event that console will emitted.
 *
 * @author Chaniwat Seangchai (chanwiat.meranote@gmail.com)
 * @since 0.1
 */
export declare type ConsoleEvent = 'start' | 'stop' | 'resume' | 'pause' | 'line' | 'SIGINT';

/**
 * Console State, Current state of the console.
 *
 * @author Chaniwat Seangchai (chanwiat.meranote@gmail.com)
 * @since 0.1
 */
export enum ConsoleState {
    NOT_START,
    READ,
    PAUSE,
    STOP,
    DISABLE,
}

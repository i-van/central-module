const framework = require('../lib/Kernel').Kernel;
const container = require('../lib/ioc/Container').Container;
const types = require('../lib/ioc/Types').Types;

new framework({
    input: process.stdin,
    output: process.stdout,
});

let c = container.get(types.APPLICATION_CONSOLE);
c.on('line', function (data) {
    c.println('Echo: ' + data);
});
c.start();

export interface BootableContract {
    boot(): boolean;
}

export interface TerminatableContract {
    terminate(): boolean;
}

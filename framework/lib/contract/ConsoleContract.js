"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ConsoleState;
(function (ConsoleState) {
    ConsoleState[ConsoleState["NOT_START"] = 0] = "NOT_START";
    ConsoleState[ConsoleState["READ"] = 1] = "READ";
    ConsoleState[ConsoleState["PAUSE"] = 2] = "PAUSE";
    ConsoleState[ConsoleState["STOP"] = 3] = "STOP";
    ConsoleState[ConsoleState["DISABLE"] = 4] = "DISABLE";
})(ConsoleState = exports.ConsoleState || (exports.ConsoleState = {}));
//# sourceMappingURL=ConsoleContract.js.map
import { TerminatableContract } from './TerminatableContract';
export interface ConsoleContract extends TerminatableContract {
    state: ConsoleState;
    start(): any;
    stop(): any;
    resume(): any;
    pause(): any;
    print(data: string): any;
    println(data: string): any;
    clearLine(): any;
    clearScreen(): any;
    addListener(event: ConsoleEvent, listener: Function): this;
    on(event: ConsoleEvent, listener: Function): this;
    once(event: ConsoleEvent, listener: Function): this;
    removeListener(event: ConsoleEvent, listener: Function): this;
}
export declare type ConsoleEvent = 'start' | 'stop' | 'resume' | 'pause' | 'line' | 'SIGINT';
export declare enum ConsoleState {
    NOT_START = 0,
    READ = 1,
    PAUSE = 2,
    STOP = 3,
    DISABLE = 4,
}

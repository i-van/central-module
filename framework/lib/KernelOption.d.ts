/// <reference types="node" />
import ReadableStream = NodeJS.ReadableStream;
import WritableStream = NodeJS.WritableStream;
export interface KernelOption {
    input?: ReadableStream;
    output?: WritableStream;
}
export declare const DefaultKernelOption: KernelOption;

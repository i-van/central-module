import { ConsoleOptions } from './console/ConsoleOptions';
import { KernelOption } from './KernelOption';
export declare class Kernel {
    private _console;
    constructor(option?: KernelOption);
    protected makeConsole(option: ConsoleOptions): void;
}

export declare const Types: {
    APPLICATION: symbol;
    APPLICATION_CONSOLE: symbol;
    BOOTABLE: symbol;
    COMMAND: symbol;
    COMMAND_MANAGER: symbol;
    CONSOLE: symbol;
    CONTROLLER: symbol;
    CONTROLLER_MANAGER: symbol;
    LOG: symbol;
    SERVICE: symbol;
    SERVICE_MANAGER: symbol;
    TERMINATABLE: symbol;
};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Types = {
    APPLICATION: Symbol.for('application'),
    APPLICATION_CONSOLE: Symbol.for('application_console'),
    BOOTABLE: Symbol.for('bootable'),
    COMMAND: Symbol.for('command'),
    COMMAND_MANAGER: Symbol.for('command_manager'),
    CONSOLE: Symbol.for('console'),
    CONTROLLER: Symbol.for('controller'),
    CONTROLLER_MANAGER: Symbol.for('controller_manager'),
    LOG: Symbol.for('log'),
    SERVICE: Symbol.for('service'),
    SERVICE_MANAGER: Symbol.for('service_manager'),
    TERMINATABLE: Symbol.for('terminatable'),
};
//# sourceMappingURL=Types.js.map
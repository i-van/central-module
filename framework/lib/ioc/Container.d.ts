import { Container as InversifyContainer } from 'inversify';
export { Types as IoC } from './Types';
export declare const Container: InversifyContainer;

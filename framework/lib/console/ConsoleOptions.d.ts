/// <reference types="node" />
import { ReadLineOptions } from 'readline';
export interface ConsoleOptions extends ReadLineOptions {
}
export declare const DefaultConsoleOption: ConsoleOptions;

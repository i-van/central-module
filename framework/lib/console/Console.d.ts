/// <reference types="node" />
import { EventEmitter } from 'events';
import { ConsoleContract, ConsoleState } from '../contract/ConsoleContract';
import { ConsoleOptions } from './ConsoleOptions';
export declare class Console extends EventEmitter implements ConsoleContract {
    private _state;
    readonly state: ConsoleState;
    private _readline;
    private _inputStream;
    private _outputStream;
    constructor(option?: ConsoleOptions);
    protected prepareReadLine(option: ConsoleOptions): void;
    protected onKeyPressInput(): void;
    protected onLineReadLine(data: string): void;
    protected onSIGINTReadLine(): void;
    terminate(): boolean;
    start(): void;
    stop(): void;
    resume(): void;
    pause(): void;
    println(data: string): void;
    print(data: string): void;
    protected clearReadLineBuffer(): void;
    clearScreen(): void;
    clearLine(): void;
}

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const events_1 = require("events");
const has = require("lodash/fp/has");
const readline_1 = require("readline");
const ConsoleContract_1 = require("../contract/ConsoleContract");
const ConsoleOptions_1 = require("./ConsoleOptions");
class Console extends events_1.EventEmitter {
    constructor(option = ConsoleOptions_1.DefaultConsoleOption) {
        super();
        this._state = ConsoleContract_1.ConsoleState.NOT_START;
        this._inputStream = option.input;
        this._outputStream = option.output;
        if (option.input === null) {
            this._state = ConsoleContract_1.ConsoleState.DISABLE;
        }
        else {
            this.prepareReadLine(option);
        }
    }
    get state() {
        return this._state;
    }
    prepareReadLine(option) {
        this._readline = readline_1.createInterface(option);
        this._readline.pause();
        if (has(option.input, 'isTTY')) {
            const input = option.input;
            input.setRawMode(true);
            input.on('keypress', this.onKeyPressInput.bind(this));
        }
        this._readline.on('line', this.onLineReadLine.bind(this));
        this._readline.on('SIGINT', this.onSIGINTReadLine.bind(this));
    }
    onKeyPressInput() {
        if (this._state !== ConsoleContract_1.ConsoleState.READ) {
            this.clearLine();
        }
    }
    onLineReadLine(data) {
        if (this._state !== ConsoleContract_1.ConsoleState.READ) {
            this._outputStream.write('\x1B[1A');
        }
        else {
            this.emit('line', data);
        }
    }
    onSIGINTReadLine() {
        this.emit('SIGINT');
        switch (this._state) {
            case ConsoleContract_1.ConsoleState.READ:
                this.clearReadLineBuffer();
                break;
            case ConsoleContract_1.ConsoleState.PAUSE:
                this.resume();
                break;
        }
    }
    terminate() {
        if (this._state !== ConsoleContract_1.ConsoleState.DISABLE && this._readline) {
            this.stop();
            this._readline.close();
        }
        return true;
    }
    start() {
        if (this._state === ConsoleContract_1.ConsoleState.DISABLE) {
            return;
        }
        if (this._state === ConsoleContract_1.ConsoleState.NOT_START) {
            this._readline.resume();
            this._state = ConsoleContract_1.ConsoleState.STOP;
        }
        this.resume();
    }
    stop() {
        if (this._state === ConsoleContract_1.ConsoleState.DISABLE) {
            return;
        }
        else {
            this._state = ConsoleContract_1.ConsoleState.STOP;
        }
    }
    resume() {
        if (this._state === ConsoleContract_1.ConsoleState.DISABLE) {
            return;
        }
        if ([ConsoleContract_1.ConsoleState.PAUSE, ConsoleContract_1.ConsoleState.STOP].indexOf(this._state) !== -1) {
            this.clearReadLineBuffer();
            this._state = ConsoleContract_1.ConsoleState.READ;
            this._readline.prompt();
        }
    }
    pause() {
        if (this._state === ConsoleContract_1.ConsoleState.DISABLE) {
            return;
        }
        else {
            this._state = ConsoleContract_1.ConsoleState.PAUSE;
        }
    }
    println(data) {
        return this.print(data + '\r\n');
    }
    print(data) {
        let lineBuffer;
        if (this._state === ConsoleContract_1.ConsoleState.READ) {
            lineBuffer = this._readline.line;
            this.clearLine();
        }
        this._outputStream.write(data);
        if (this._state === ConsoleContract_1.ConsoleState.READ) {
            this._readline.prompt();
            this._readline.write(lineBuffer);
        }
    }
    clearReadLineBuffer() {
        if (this._state !== ConsoleContract_1.ConsoleState.DISABLE) {
            this._readline.line = '';
        }
    }
    clearScreen() {
        this.clearReadLineBuffer();
        this._outputStream.write('\x1Bc');
    }
    clearLine() {
        this.clearReadLineBuffer();
        this._outputStream.write('\x1B[2K\x1B[1000D');
    }
}
exports.Console = Console;
//# sourceMappingURL=Console.js.map
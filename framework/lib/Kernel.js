"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const merge = require("lodash/fp/merge");
const Console_1 = require("./console/Console");
const Container_1 = require("./ioc/Container");
const KernelOption_1 = require("./KernelOption");
class Kernel {
    constructor(option = {}) {
        this._console = null;
        option = merge(KernelOption_1.DefaultKernelOption, option);
        if (process.env.NO_CONSOLE !== 'true') {
            this.makeConsole({
                input: option.input,
                output: option.output,
            });
        }
    }
    makeConsole(option) {
        let instance = this._console = new Console_1.Console(option);
        Container_1.Container.bind(Container_1.IoC.APPLICATION_CONSOLE).toConstantValue(instance);
        Container_1.Container.bind(Container_1.IoC.CONSOLE).toConstantValue(instance);
    }
}
exports.Kernel = Kernel;
//# sourceMappingURL=Kernel.js.map